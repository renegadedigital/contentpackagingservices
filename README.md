# Renegade Content Packaging Services

Project provides services that can be integrated into console commands for project builds. Some vendors (iRep etc.) require assets to be packaged in very specific ways. Also provides methods for automating screenshot pdf generation of projects for client review.

## Project Setup

Add the following lines to composer.json

```
"repositories": [
    {
        "type": "package",
        "package": {
            "name": "renegade/contentpackagingservices",
            "version": "1.1.0",
            "source": {
                "url": "https://bitbucket.org/renegadedigital/contentpackagingservices.git",
                "type": "git",
                "reference": "master"
            },
            "autoload": {
                "classmap": ["src"]
            }
        }
    }
],
```

and then add to composer.json dependencies (now that resource has been defined)

```
"require": {
          "renegade/contentpackagingservices": "1.1.0"
}
```

in the Silex Application add this line

```
$app->register(new \Renegade\VendorServices\RenegadeServiceProvider());
```

Then use wherever, please change servicename to the name of the actual service (irep etc.)

```
$app['renegade.contentpackagingservices.servicename']->build($input, $output);
```

## License

GPL v2
