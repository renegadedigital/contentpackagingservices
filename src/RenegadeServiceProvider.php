<?php

namespace Renegade\ContentPackagingServices;

use Renegade\ContentPackagingServices\Service\Carapace;
use Renegade\ContentPackagingServices\Service\iRep;
use Renegade\ContentPackagingServices\Service\Flat;
use Silex\Application;
use Silex\ServiceProviderInterface;

class RenegadeServiceProvider implements ServiceProviderInterface {
    public function register(Application $app) {
        $app['renegade.contentpackagingservices.irep'] =  new iRep($app['console'], $app['twig'],  $app['filesystem'], $app['tlherr.webkit2png'], $app['tcpdf'], $app['config'], $app['directory']);
        $app['renegade.contentpackagingservices.carapace'] =  new Carapace($app['console'], $app['twig'],  $app['filesystem'], $app['tlherr.webkit2png'], $app['tcpdf'], $app['config'], $app['directory']);
        $app['renegade.contentpackagingservices.flat'] =  new Flat($app['console'], $app['twig'],  $app['filesystem'], $app['config'], $app['directory']);
        return $app;
    }

    public function boot(Application $app) {}
}
