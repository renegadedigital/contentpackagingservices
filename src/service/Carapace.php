<?php

namespace Renegade\ContentPackagingServices\Service;

use Renegade\ContentPackagingServices\Helper\ArchiveHelper;
use Renegade\ContentPackagingServices\Helper\DirectoryHelper;
use Renegade\ContentPackagingServices\Helper\MessagesHelper;
use Renegade\ContentPackagingServices\Helper\NetworkHelper;
use Renegade\ContentPackagingServices\RenegadeServiceInterface;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Console\Helper\ProgressHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use DirectoryIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use DOMDocument;
use DOMElement;
use Symfony\Component\HttpFoundation\File\File;
use tlherr\webkit2png;


/**
 * Class iRep
 *
 * @author Thomas Herr
 * @version 1.0.0
 *
 * This class implements the required methods as defined in the service interface for an iRep application
 * iRep applications require:
 *
 * - Each file/folder/zip must be language prefixed (en_example_file.zip)
 * - Series of zipped packages. Each package must be named the same as the html file within it (minus extension name obviously)
 * - Each zip will contain 2 screenshots, one fullsized screenshot and one thumbnail. These will have the same name as the zip
 *
 * @package Renegade\ContentPackagingServices\Service
 */
class Carapace implements RenegadeServiceInterface {

    protected $console;
    protected $filesystem;
    protected $webkit2png;
    protected $tcpdf;
    protected $config;
    protected $directory;
    protected $twig;

    public $archiveHelper;
    public $directoryHelper;
    public $messagesHelper;
    public $networkHelper;

    /**
     * @param $console
     * @param $twig
     * @param \Symfony\Component\Filesystem\Filesystem $filesystem
     * @param $webkit2png
     * @param \TCPDF $tcpdf
     * @param $config
     * @param $directory                string      Application Path (console.php location)
     */
    public function __construct($console, $twig, Filesystem $filesystem, webkit2png $webkit2png, \TCPDF $tcpdf, $config, $directory) {
        $this->console = $console;
        $this->twig = $twig;
        $this->filesystem = $filesystem;
        $this->webkit2png = $webkit2png;
        $this->tcpdf = $tcpdf;
        $this->config = $config;
        $this->directory = $directory;

        $this->archiveHelper = new ArchiveHelper();
        $this->directoryHelper = new DirectoryHelper($filesystem, $config, $directory);
        $this->messagesHelper = new MessagesHelper();
        $this->networkHelper = new NetworkHelper();
    }

    public function build(InputInterface $input, OutputInterface $output) {
        $language = strtolower($input->getOption('lang'));
        $localizedPath = $this->directoryHelper->init($language, 'build');
        $output->writeln($this->messagesHelper->success_message(sprintf("* Scanning for template files matching language type: %s", $language)));
        $pageTemplatesDirectory = new RecursiveDirectoryIterator(sprintf('%s/views/%s/pages', $this->directory, $language));
        $srcDirectory = new DirectoryIterator($this->directory);
        $templateIterator = new RecursiveIteratorIterator($pageTemplatesDirectory);
        $templateIterator->setFlags(RecursiveDirectoryIterator::SKIP_DOTS);

        /**
         * @var $progress ProgressHelper
         */
        $progress = $this->console->getHelperSet()->get('progress');
        $progress->setFormat(ProgressHelper::FORMAT_VERBOSE_NOMAX);
        $progress->start($output);

        // Copy common assets
        $output->writeln($this->messagesHelper->success_message("* Copying common assets"));
        foreach ($srcDirectory as $srcFile) {
            // Exclude the views, parent, current directory, and images (we'll only copy what was used)
            if ($srcFile->isDir() && array_search($srcFile->getFilename(), array('views', 'support', 'img', 'data', '.', '..')) === FALSE) {
                $output->writeln($this->messagesHelper->success_message(sprintf("* Copying %s directories", $srcFile->getFilename())));
                $this->filesystem->mkdir(sprintf('%s/%s', $localizedPath, $srcFile->getFilename()));
                $this->filesystem->mirror(sprintf('%s/%s', $this->directory, $srcFile->getFilename()), sprintf('%s/%s', $localizedPath, $srcFile->getFilename()));
            }
        }

        $localizedDirs = array('data', 'support');
        foreach ($localizedDirs as $localizedDir) {
            $dataDir = sprintf('%s/%s/%s', $this->directory, $localizedDir, $language);
            if ($this->filesystem->exists($dataDir)) {
                $destDir = sprintf('%s/%s', $localizedPath, $localizedDir);
                $this->filesystem->mkdir($destDir);
                $this->filesystem->mirror($dataDir, $destDir);
            }
        }

        foreach($templateIterator as $template_file) {
            /**
             * @var $template_file DirectoryIterator
             */
            $filename = $template_file->getBasename();
            if ($template_file->getExtension() == "twig") {
                $slideName = sprintf('%s%s', !empty($this->config['application']['prefix']) ? $this->config['application']['prefix'] . '_': '', $template_file->getBasename('.html.twig'));
                $localizedSlideName = sprintf("%s_%s", $language, $slideName);
                $localizedFilename = sprintf("%s.html", $localizedSlideName);

                $this->directoryHelper->save_page(
                    $this->twig->render(
                        sprintf('/%s/pages/%s', $language, $filename),
                        array(
                            'preview' => false,
                            'language' => $language,
                        )
                    ),
                    $localizedPath,
                    $localizedFilename
                );

                $doc = new \DOMDocument();
                $doc->loadHTMLFile(sprintf('%s/%s', $localizedPath, $localizedSlideName . '.html'));

                // Copy images
                $imageTags = $doc->getElementsByTagName('img');
                $output->writeln($this->messagesHelper->notification_message(sprintf('Found %s img resources', count($imageTags))));
                foreach($imageTags as $tag) {
                    $actualPath = str_replace("resources/package/", "", $tag->getAttribute('src'));
                    $output->writeln($this->messagesHelper->notification_message(sprintf('Copying %s from %s to %s/%s', $tag->getAttribute('src'), $this->directory, $localizedPath, $tag->getAttribute('src'))));
                    $this->filesystem->copy(sprintf('%s/%s', $this->directory, $actualPath), sprintf('%s/%s', $localizedPath, $actualPath));
                }

                $progress->advance();
            }
        }
        $progress->finish();
        $output->writeln($this->messagesHelper->success_message('Build Completed'));
    }

    public function screenshot(InputInterface $input, OutputInterface $output) {
        $language = $input->getOption('lang');
        $this->directoryHelper->init($input->getOption('lang'), 'screenshots');
        $directory = new RecursiveDirectoryIterator(sprintf('%s/views/%s/pages', $this->directory, $language));
        $iterator = new RecursiveIteratorIterator($directory);
        $iterator->setFlags(RecursiveDirectoryIterator::SKIP_DOTS);

        $domain = sprintf('http://%s', $this->config['application']['url']);
//        if(!$this->networkHelper->isDomainAvailible($domain)) {
//            throw new \Exception('URL Unavailable to screenshot');
//        }

        /**
         * @var $progress ProgressHelper
         */
        $progress = $this->console->getHelperSet()->get('progress');
        $progress->setFormat(ProgressHelper::FORMAT_VERBOSE_NOMAX);
        $progress->start($output);

        $thumbDir = $this->directoryHelper->get_localized_dir('build', $language) . '/thumbs';


        if (!$this->filesystem->exists($thumbDir)) {
            $this->filesystem->mkdir($thumbDir);
        }

        $thumbDir = realpath($thumbDir);

        /**
         * @var \SplFileInfo $fileInfo
         */
        foreach($iterator as $fileInfo) {
            // Make sure its not a dot file
            if (!$fileInfo->isDir() && array_search($fileInfo->getFilename(), array('.', '..', '.DS_Store')) === FALSE && preg_match("/^page/", $fileInfo->getFilename())) {
                $screenshot = new $this->webkit2png;
                $screenshot->setUrl(sprintf( 'http://%s/%s/view/%s?screenshot', $this->config['application']['url'], $language, $fileInfo->getFilename()));
                $filename = sprintf('%s_%s%s', $language, !empty($this->config['application']['prefix']) ? $this->config['application']['prefix'] . '_': '', $fileInfo->getBasename('.html.twig'));
                $file_location = realpath(sprintf('%s', $this->directoryHelper->get_localized_dir('screenshots', $language)));
                $screenshot->setOptions(array(
                        'filename' => $file_location . '/' . $filename,
                        'clipped' => true,
                        'clipped-height' => 768,
                        'clipped-width' => 1024,
                        'scale' => 1
                    )
                );
                $output->writeln(sprintf("\nExecuting %s", $this->messagesHelper->notification_message($screenshot->getQuery())));
                $screenshot->getImage();

                if ($this->filesystem->exists($thumbDir)) {
                    $imagemagic = new \Imagick(sprintf('%s/%s-clipped.png', $file_location, $filename));
                    $imagemagic->setimageformat('jpg');
                    $imagemagic->setcompressionquality(100);
                    $imagemagic->resizeimage(176, 132, \Imagick::FILTER_CATROM, 1, false);
                    $imagemagic->writeimage(sprintf('%s/%s.jpg', $thumbDir, $filename));
                }
                $progress->advance();
            }
        }
        $progress->finish();
        $output->writeln($this->messagesHelper->success_message('Operation Complete'));
    }

    public function pdf(InputInterface $input, OutputInterface $output) {
        $this->directoryHelper->init($input->getOption('lang'), 'pdf');

        $file = sprintf('%s/%s-%s-pdf.pdf', $this->directoryHelper->get_localized_dir('pdf', $input->getOption('lang')), $input->getOption('lang'), time());
        $directory = $this->directoryHelper->get_localized_dir('screenshots', $input->getOption('lang'));
        $iterator = new DirectoryIterator($directory);

        /**
         * @var $pdf \TCPDF
         */
        $pdf = new $this->tcpdf('L', 'px', 'Letter', true, 'UTF-8', false);
        $pdf->SetCreator('Renegade Digital Media Inc.');
        $pdf->SetAuthor('Renegade Digital Media Inc.');
        $pdf->SetTitle(sprintf('%s-%s', $input->getOption('lang'), time()));
        $pdf->setPrintFooter(false);
        $pdf->setPrintHeader(false);
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(0);
        $pdf->SetFooterMargin(0);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->setJPEGQuality(100);

        foreach($iterator as $fileInfo) {
            /**
             * @var $fileInfo DirectoryIterator
             */
            if($fileInfo->getExtension()=='png') {
                $pdf->AddPage();
                $pdf->SetAutoPageBreak(false, 0);
                $pdf->Image(sprintf('%s/%s', $fileInfo->getPathInfo()->getRealPath(), $fileInfo->getFilename()) , 0, 0, 792, 612, '', '', '', false, 72, '', false, false, 0);
                $pdf->SetAutoPageBreak($pdf->getAutoPageBreak(), $pdf->getBreakMargin());
                $pdf->setPageMark();
                $pdf->writeHTML('&nbsp;', true, false, true, false, '');

                $output->writeln($this->messagesHelper->success_message(sprintf('  Image: %s added to pdf!', $fileInfo->getFilename())));
            }
        }

        $output->writeln($this->messagesHelper->success_message(sprintf('Saving PDF to file %s', $file)));
        $pdf->Output($file , 'F');

        if(filesize($file)) {
            $output->writeln($this->messagesHelper->success_message('Operation Successful!'));
        }
    }

    public function package(InputInterface $input, OutputInterface $output) {
        $language = $input->getOption('lang');
        $output->writeln($this->messagesHelper->success_message(sprintf("* Scanning for template files in directory: %s matching language type: %s", $this->directoryHelper->get_localized_dir('build', $language), $language)));

        $iterator = new RecursiveDirectoryIterator($this->directoryHelper->get_localized_dir('build', $language));
        $iterator->setFlags(RecursiveDirectoryIterator::SKIP_DOTS);

        /**
         * @var $progress ProgressHelper
         */
        $progress = $this->console->getHelperSet()->get('progress');
        $progress->setFormat(ProgressHelper::FORMAT_VERBOSE_NOMAX);
        $progress->start($output);

        foreach($iterator as $directory) {
            /**
             * @var $directory DirectoryIterator
             */
            if($directory->isDir()) {
                $output->writeln($this->messagesHelper->notification_message(sprintf('Zipping Directory: %s. Saving to: %s', $directory->getFilename(), sprintf('%s/%s.zip', $this->directoryHelper->get_localized_dir('dist', $input->getOption('lang')), $directory->getFilename()))));
                $zip = new ArchiveHelper();
                $zip->Zip($directory->getRealPath(), sprintf('%s/%s.zip', $this->directoryHelper->get_localized_dir('dist', $input->getOption('lang')), $directory->getFilename()));
                $progress->advance();
            }
        }
        $progress->finish();
    }

}
