<?php

namespace Renegade\ContentPackagingServices\Service;

use Renegade\ContentPackagingServices\Helper\ArchiveHelper;
use Renegade\ContentPackagingServices\Helper\DirectoryHelper;
use Renegade\ContentPackagingServices\Helper\MessagesHelper;
use Renegade\ContentPackagingServices\Helper\NetworkHelper;
use Renegade\ContentPackagingServices\RenegadeServiceInterface;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Console\Helper\ProgressHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use DirectoryIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use DOMDocument;
use DOMElement;
use tlherr\webkit2png;


/**
 * Class iRep
 *
 * @author Thomas Herr
 * @version 1.0.0
 *
 * This class implements the required methods as defined in the service interface for an iRep application
 * iRep applications require:
 *
 * - Each file/folder/zip must be language prefixed (en_example_file.zip)
 * - Series of zipped packages. Each package must be named the same as the html file within it (minus extension name obviously)
 * - Each zip will contain 2 screenshots, one fullsized screenshot and one thumbnail. These will have the same name as the zip
 *
 * @package Renegade\ContentPackagingServices\Service
 */
class iRep implements RenegadeServiceInterface {

    public $archiveHelper;
    public $directoryHelper;
    public $messagesHelper;
    public $networkHelper;
    protected $console;
    protected $filesystem;
    protected $webkit2png;
    protected $tcpdf;
    protected $config;
    protected $directory;
    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * @param $console
     * @param $twig
     * @param \Symfony\Component\Filesystem\Filesystem $filesystem
     * @param $webkit2png
     * @param \TCPDF $tcpdf
     * @param $config
     * @param $directory                string      Application Path (console.php location)
     */
    public function __construct($console, $twig, Filesystem $filesystem, webkit2png $webkit2png, \TCPDF $tcpdf, $config, $directory) {
        $this->console = $console;
        $this->twig = $twig;
        $this->filesystem = $filesystem;
        $this->webkit2png = $webkit2png;
        $this->tcpdf = $tcpdf;
        $this->config = $config;
        $this->directory = $directory;

        $this->archiveHelper = new ArchiveHelper();
        $this->directoryHelper = new DirectoryHelper($filesystem, $config, $directory);
        $this->messagesHelper = new MessagesHelper();
        $this->networkHelper = new NetworkHelper();
    }

    public function build(InputInterface $input, OutputInterface $output) {
        $language = $input->getOption('lang');
        $this->directoryHelper->init($language, 'build');
        $currentPageIndex = 0;
        $output->writeln($this->messagesHelper->success_message(sprintf("* Scanning for template files matching language type: %s", $language)));
        $directory = new RecursiveDirectoryIterator(sprintf('%s/views/%s/pages', $this->directory, $language));
        $iterator = new RecursiveIteratorIterator($directory);
        $prefix = (is_array($this->config['application']['prefix'])?$this->config['application']['prefix'][$language]:$this->config['application']['prefix']);

        $slidePrefix = sprintf('%s_slide', $prefix);

        /**
         * @var $iterator RecursiveIteratorIterator
         */
        $iterator->setFlags(RecursiveDirectoryIterator::SKIP_DOTS);

        $pageIndexes = array();
        $slideIds = array();

        // Build an index of slide names that we can use to determine previous and next pages from
        foreach ($iterator as $template_file) {
            $baseName = $template_file->getBasename('.html.twig');
            $pageIndexes[] = $baseName;
            preg_match('/slide([\d]+)/', $baseName, $slideOrder);
            $slideIds[$baseName] = $slideOrder[1];
        }

        // Rewind the directory iterator
        $iterator->rewind();

        /**
         * @var $progress ProgressHelper
         */
        $progress = $this->console->getHelperSet()->get('progress');
        $progress->setFormat(ProgressHelper::FORMAT_VERBOSE_NOMAX);
        $progress->start($output);

        $lastPageIndex = count($pageIndexes) - 1;
        foreach($iterator as $template_file) {
            /**
             * @var $template_file DirectoryIterator
             */
            $filename = $template_file->getBasename();
            $slideName = sprintf('%s_%s', $prefix, $template_file->getBasename('.html.twig'));
            $localizedSlideName = $slideName;
            $localizedFilename = sprintf("%s.html", $localizedSlideName);
            $localizedCtlFilename = sprintf("%s.ctl", $localizedSlideName);
            $localizedZipFilename = sprintf("%s.zip", $localizedSlideName);

            // Figure out what's before and after this page in the array
            $prevSlide = $pageIndexes[$currentPageIndex > 0 ? $currentPageIndex - 1 : $lastPageIndex];
            $nextSlide = $pageIndexes[$currentPageIndex < $lastPageIndex ? $currentPageIndex + 1 : 0];

            // Create directory, utilize prefix from the config
            $sub_dir = sprintf('%s/%s', $this->directoryHelper->get_localized_dir('build', $language), $localizedSlideName);
            $this->filesystem->mkdir($sub_dir);


            $params = array(
              'is_relative' => $input->getOption('assets_relative'),
              'page_number' => $currentPageIndex + 1,
              'prefix' => $slidePrefix,
              'prev' => $slideIds[$prevSlide],
              'next' => $slideIds[$nextSlide],
              'page' => sprintf('page-%s', preg_replace('#\D#', '', $filename)),
              'build_type' => 'irep'
            );

            $this->directoryHelper->save_page(
                $this->twig->render(
                    sprintf('/%s/pages/%s', $language, $filename),
                    array(
                        'is_relative' => $input->getOption('assets_relative'),
                        'page_number' => $currentPageIndex + 1,
                        'prefix' => $slidePrefix,
                        'prev' => $slideIds[$prevSlide],
                        'next' => $slideIds[$nextSlide],
                        'page' => sprintf('page-%s', preg_replace('#\D#', '', $filename)),
                        'build_type' => 'irep'
                    )
                ),
                $sub_dir,
                $localizedFilename
            );

            // Load up the document so we can pull some values from it
            $doc = new \DOMDocument();
            $doc->loadHTMLFile(sprintf('%s/%s', $sub_dir, $localizedSlideName . '.html'));

            // Get title of page
            $output->writeln($this->messagesHelper->success_message(sprintf("\n* Searching document [%s] for title", $localizedSlideName)));
            $titleTag = $doc->getElementsByTagName('title')->item(0);
            $metaTags = $doc->getElementsByTagName('meta');

            foreach($metaTags as $meta) {
                if (strtolower($meta->getAttribute('name')) == "description") {
                    $description = htmlspecialchars_decode($meta->getAttribute('content'));
                }
            }

            $this->directoryHelper->save_page(
                $this->twig->render(
                    sprintf("/%s/general/ctl_base.ctl.twig", $language),
                    array(
                        'name' => $titleTag ? $titleTag->textContent : $localizedSlideName,
                        'description' => $description ? $description : $localizedSlideName,
                        'filename' => $localizedZipFilename,
                        'order' => $slideIds[$pageIndexes[$currentPageIndex]]
                    )
                ),
                $this->directoryHelper->get_localized_dir('build', $language),
                $localizedCtlFilename
            );

            $output->writeln($this->messagesHelper->success_message(sprintf("\n* Searching document [%s] for resources", $localizedSlideName)));

            /**
             * Copy CSS
             */
            $this->filesystem->mkdir(sprintf('%s/%s', $sub_dir, 'css'));
            $linkTags = $doc->getElementsByTagName('link');
            $output->writeln($this->messagesHelper->notification_message(sprintf('Found %s css resources', $linkTags->length)));
            foreach($linkTags as $tag) {

                /**
                 * @var $tag DOMElement
                 */
                $resourceFilename = sprintf('%s/%s', $this->directory, $tag->getAttribute('href'));
                if (file_exists($resourceFilename)) {
                    $output->writeln(
                      $this->messagesHelper->notification_message(
                        sprintf(
                          'Copying %s from %s to %s/%s',
                          $tag->getAttribute('href'),
                          $this->directory,
                          $sub_dir,
                          $tag->getAttribute('href')
                        )
                      )
                    );
                    $this->filesystem->copy(
                      sprintf(
                        '%s/%s',
                        $this->directory,
                        $tag->getAttribute('href')
                      ),
                      sprintf(
                        '%s/%s',
                        $sub_dir,
                        $tag->getAttribute('href')
                      )
                    );
                } else {
                    $output->writeln($this->messagesHelper->error_message(sprintf("Unable to find %s to copy", $resourceFilename)));
                }
            }

            /**
             * Copy javascript files reference in <script> tags
             */
            $this->filesystem->mkdir(sprintf('%s/%s', $sub_dir, 'js'));
            $scriptTags = $doc->getElementsByTagName('script');
            $output->writeln($this->messagesHelper->notification_message(sprintf('Found %s js resources', $scriptTags->length)));
            foreach($scriptTags as $tag) {
                /**
                 * @var $tag DOMElement
                 */
                $resourceFilename = sprintf('%s/%s', $this->directory, $tag->getAttribute('src'));
                if (file_exists($resourceFilename)) {
                    $output->writeln(
                      $this->messagesHelper->notification_message(
                        sprintf(
                          'Copying %s from %s to %s/%s',
                          $tag->getAttribute('src'),
                          $this->directory,
                          $sub_dir,
                          $tag->getAttribute('src')
                        )
                      )
                    );
                    $this->filesystem->copy(
                      $resourceFilename,
                      sprintf('%s/%s', $sub_dir, $tag->getAttribute('src'))
                    );
                } else {
                    $output->writeln($this->messagesHelper->error_message(sprintf("Unable to find %s to copy", $resourceFilename)));
                }
            }

            /**
             * Copy all files located in src/assets/
             */
            $this->filesystem->mkdir(sprintf('%s/%s', $sub_dir, 'assets'));
            $this->filesystem->mirror(sprintf('%s/%s', $this->directory, 'assets'), sprintf('%s/%s', $sub_dir, 'assets'));

            /**
             * Copy images referenced in <img> tags
             */
            $this->filesystem->mkdir(sprintf('%s/%s', $sub_dir, 'img'));
            $imageTags = $doc->getElementsByTagName('img');
            $output->writeln($this->messagesHelper->notification_message(sprintf('Found %s img resources', $imageTags->length)));
            foreach($imageTags as $tag) {
                /**
                 * @var $tag DOMElement
                 */
                $output->writeln($this->messagesHelper->notification_message(sprintf('Moving %s from %s to %s/%s', $tag->getAttribute('src'), $this->directory, $sub_dir, $tag->getAttribute('src'))));
                $this->filesystem->copy(sprintf('%s/%s', $this->directory, $tag->getAttribute('src')), sprintf('%s/%s', $sub_dir, $tag->getAttribute('src')));
            }

            $progress->advance();

            $currentPageIndex = $currentPageIndex + 1;
        }
        $progress->finish();
        $output->writeln($this->messagesHelper->success_message('Build Completed'));
    }

    public function screenshot(InputInterface $input, OutputInterface $output) {
        $language = $input->getOption('lang');
        $this->directoryHelper->init($input->getOption('lang'), 'screenshots');
        $prefix = (is_array($this->config['application']['prefix'])?$this->config['application']['prefix'][$language]:$this->config['application']['prefix']);
        $directory = new RecursiveDirectoryIterator(sprintf('%s/views/%s/pages', $this->directory, $language));
        $iterator = new RecursiveIteratorIterator($directory);
        $iterator->setFlags(RecursiveDirectoryIterator::SKIP_DOTS);

        if(!$this->networkHelper->isDomainAvailible(sprintf( 'http://%s', $this->config['application']['url']))) {
            throw new \Exception('URL Unavailable to screenshot');
        }

        /**
         * @var $progress ProgressHelper
         */
        $progress = $this->console->getHelperSet()->get('progress');
        $progress->setFormat(ProgressHelper::FORMAT_VERBOSE_NOMAX);
        $progress->start($output);

        foreach($iterator as $fileInfo) {
            $screenshot = new $this->webkit2png;
            $screenshot->setUrl(sprintf( 'http://%s/%s/view/%s', $this->config['application']['url'], $language, $fileInfo->getFilename()));
            $filename = sprintf('%s_%s', $prefix, $fileInfo->getBasename('.html.twig'));
            $file_location = sprintf('%s/%s/%s', $this->directoryHelper->get_localized_dir('build', $language), $filename, $filename);
            // $file_location = sprintf('%s/%s', $this->directoryHelper->get_localized_dir('screenshots', $language), $filename);
            $screenshot->setOptions(array(
                    'filename' => $file_location,
                    'width' => 1024,
                    'height' => 768,
                    'fullsize' => true,
                    'thumb' => true
                )
            );

            $output->writeln(sprintf("\nExecuting %s", $this->messagesHelper->notification_message($screenshot->getQuery())));
            $output->writeln(sprintf("\nWriting %s", $file_location));
            $screenshot->getImage();



            // $imagemagic = new Imagick(sprintf('%s-full.png', $file_location));

            // $imagemagic->setimageformat('jpg');
            // $imagemagic->setcompressionquality(60);
            // $imagemagic->resizeimage(1024, 768, \Imagick::FILTER_CATROM, 1, false);
            // $imagemagic->writeimage(sprintf('%s/%s/%s-full.jpg', $this->directoryHelper->get_localized_dir('build', $language), $filename, $filename));
            // $imagemagic->resizeimage(120, 90, \Imagick::FILTER_CATROM, 1, false);
            // $imagemagic->writeimage(sprintf('%s/%s/%s-thumb.jpg', $this->directoryHelper->get_localized_dir('build', $language), $filename, $filename));

            // $progress->advance();
        }
        $progress->finish();
        $output->writeln($this->messagesHelper->success_message('Operation Complete'));
    }

    public function pdf(InputInterface $input, OutputInterface $output) {
        $this->directoryHelper->init($input->getOption('lang'), 'pdf');

        $file = sprintf('%s/%s-%s-pdf.pdf', $this->directoryHelper->get_localized_dir('pdf', $input->getOption('lang')), $input->getOption('lang'), time());
        $directory = $this->directoryHelper->get_localized_dir('screenshots', $input->getOption('lang'));
        $iterator = new DirectoryIterator($directory);

        /**
         * @var $pdf \TCPDF
         */
        $pdf = new $this->tcpdf('L', 'px', 'Letter', true, 'UTF-8', false);
        $pdf->SetCreator('Renegade Digital Media Inc.');
        $pdf->SetAuthor('Renegade Digital Media Inc.');
        $pdf->SetTitle(sprintf('%s-%s', $input->getOption('lang'), time()));
        $pdf->setPrintFooter(false);
        $pdf->setPrintHeader(false);
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(0);
        $pdf->SetFooterMargin(0);
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->setJPEGQuality(100);

        foreach($iterator as $fileInfo) {
            /**
             * @var $fileInfo DirectoryIterator
             */
            if($fileInfo->getExtension()=='png') {
                $pdf->AddPage();
                $pdf->SetAutoPageBreak(false, 0);
                $pdf->Image(sprintf('%s/%s', $fileInfo->getPathInfo()->getRealPath(), $fileInfo->getFilename()) , 0, 0, 792, 612, '', '', '', false, 72, '', false, false, 0);
                $pdf->SetAutoPageBreak($pdf->getAutoPageBreak(), $pdf->getBreakMargin());
                $pdf->setPageMark();
                $pdf->writeHTML('&nbsp;', true, false, true, false, '');

                $output->writeln($this->messagesHelper->success_message(sprintf('  Image: %s added to pdf!', $fileInfo->getFilename())));
            }
        }

        $output->writeln($this->messagesHelper->success_message(sprintf('Saving PDF to file %s', $file)));
        $pdf->Output($file , 'F');

        if(filesize($file)) {
            $output->writeln($this->messagesHelper->success_message('Operation Successful!'));
        }
    }

    public function package(InputInterface $input, OutputInterface $output) {
        $language = $input->getOption('lang');
        $output->writeln($this->messagesHelper->success_message(sprintf("* Scanning for template files in directory: %s matching language type: %s", $this->directoryHelper->get_localized_dir('build', $language), $language)));

        $iterator = new RecursiveDirectoryIterator($this->directoryHelper->get_localized_dir('build', $language));
        $iterator->setFlags(RecursiveDirectoryIterator::SKIP_DOTS);

        /**
         * @var $progress ProgressHelper
         */
        $progress = $this->console->getHelperSet()->get('progress');
        $progress->setFormat(ProgressHelper::FORMAT_VERBOSE_NOMAX);
        $progress->start($output);

        foreach($iterator as $directory) {
            /**
             * @var $directory DirectoryIterator
             */
            if($directory->isDir()) {
                $output->writeln($this->messagesHelper->notification_message(sprintf('Zipping Directory: %s. Saving to: %s', $directory->getFilename(), sprintf('%s/%s.zip', $this->directoryHelper->get_localized_dir('dist', $input->getOption('lang')), $directory->getFilename()))));
                $zip = new ArchiveHelper();
                $zip->Zip($directory->getRealPath(), sprintf('%s/%s.zip', $this->directoryHelper->get_localized_dir('dist', $input->getOption('lang')), $directory->getFilename()));
                $progress->advance();
            }
        }
        $progress->finish();
    }

}
