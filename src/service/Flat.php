<?php

namespace Renegade\ContentPackagingServices\Service;

use Renegade\ContentPackagingServices\Helper\ArchiveHelper;
use Renegade\ContentPackagingServices\Helper\DirectoryHelper;
use Renegade\ContentPackagingServices\Helper\MessagesHelper;
use Renegade\ContentPackagingServices\Helper\NetworkHelper;
use Renegade\ContentPackagingServices\RenegadeServiceInterface;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Console\Helper\ProgressHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use DirectoryIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

/**
 * Class iRep
 *
 * @author Thomas Herr
 * @version 1.0.0
 *
 * This class implements the required methods as defined in the service interface for an iRep application
 * iRep applications require:
 *
 * - Each file/folder/zip must be language prefixed (en_example_file.zip)
 * - Series of zipped packages. Each package must be named the same as the html file within it (minus extension name obviously)
 * - Each zip will contain 2 screenshots, one fullsized screenshot and one thumbnail. These will have the same name as the zip
 *
 * @package Renegade\ContentPackagingServices\Service
 */
class Flat implements RenegadeServiceInterface {

    public $archiveHelper;
    public $directoryHelper;
    public $messagesHelper;
    public $networkHelper;
    protected $console;
    protected $filesystem;
    protected $config;
    protected $directory;
    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * @param $console
     * @param $twig
     * @param \Symfony\Component\Filesystem\Filesystem $filesystem
     * @param $config
     * @param $directory                string      Application Path (console.php location)
     */
    public function __construct($console, $twig, Filesystem $filesystem, $config, $directory) {
        $this->console = $console;
        $this->twig = $twig;
        $this->filesystem = $filesystem;
        $this->config = $config;
        $this->directory = $directory;

        $this->archiveHelper = new ArchiveHelper();
        $this->directoryHelper = new DirectoryHelper($filesystem, $config, $directory);
        $this->messagesHelper = new MessagesHelper();
        $this->networkHelper = new NetworkHelper();
    }

    public function build(InputInterface $input, OutputInterface $output) {
        $language = $input->getOption('lang');
        $this->directoryHelper->init($language, 'build');
        $currentPageIndex = 0;
        $output->writeln($this->messagesHelper->success_message(sprintf("* Scanning for template files matching language type: %s", $language)));
        $directory = new RecursiveDirectoryIterator(sprintf('%s/views/%s/pages', $this->directory, $language));
        $iterator = new RecursiveIteratorIterator($directory);
        $prefix = (is_array($this->config['application']['prefix'])?$this->config['application']['prefix'][$language]:$this->config['application']['prefix']);
        $slidePrefix = sprintf('%s_slide', $prefix);
        $buildDirectory = $this->directoryHelper->get_localized_dir('build', $language);

        // Copy assets to the build directory
        $srcDirectory = new DirectoryIterator($this->directory);
        $output->writeln($this->messagesHelper->success_message("* Copying common assets"));
        foreach ($srcDirectory as $srcFile) {
            // Exclude the views, parent, current directory
            if ($srcFile->isDir() && array_search($srcFile->getFilename(), array('views',  '.', '..')) === FALSE) {
                $filePath = sprintf('%s/%s', $buildDirectory, $srcFile->getFilename());
                $output->writeln($this->messagesHelper->success_message(sprintf("* Copying %s directory", $srcFile->getFilename())));
                $this->filesystem->mkdir($filePath);
                $this->filesystem->mirror(sprintf('%s/%s', $this->directory, $srcFile->getFilename()), $filePath);
            }
        }

        /**
         * @var $iterator RecursiveIteratorIterator
         */
        $iterator->setFlags(RecursiveDirectoryIterator::SKIP_DOTS);

        $pageIndexes = array();
        $slideIds = array();

        // Build an index of slide names that we can use to determine previous and next pages from
        foreach ($iterator as $template_file) {
            $baseName = $template_file->getBasename('.html.twig');
            $pageIndexes[] = $baseName;
            preg_match('/slide([\d]+)/', $baseName, $slideOrder);
            $slideIds[$baseName] = $slideOrder[1];
        }

        // Rewind the directory iterator
        $iterator->rewind();

        /**
         * @var $progress ProgressHelper
         */
        $progress = $this->console->getHelperSet()->get('progress');
        $progress->setFormat(ProgressHelper::FORMAT_VERBOSE_NOMAX);
        $progress->start($output);

        $lastPageIndex = count($pageIndexes) - 1;
        foreach($iterator as $template_file) {
            /**
             * @var $template_file DirectoryIterator
             */
            $filename = $template_file->getBasename();
            $slideName = sprintf('%s_%s', $prefix, $template_file->getBasename('.html.twig'));
            $localizedSlideName = sprintf("%s", $slideName);
            $localizedFilename = sprintf("%s.html", $localizedSlideName);

            // Figure out what's before and after this page in the array
            $prevSlide = $pageIndexes[$currentPageIndex > 0 ? $currentPageIndex - 1 : $lastPageIndex];
            $nextSlide = $pageIndexes[$currentPageIndex < $lastPageIndex ? $currentPageIndex + 1 : 0];

            $this->directoryHelper->save_page(
                $this->twig->render(
                    sprintf('/%s/pages/%s', $language, $filename),
                    array(
                        'is_relative' => $input->getOption('assets_relative'),
                        'page_number' => $currentPageIndex + 1,
                        'prefix' => $slidePrefix,
                        'prev' => $slideIds[$prevSlide],
                        'next' => $slideIds[$nextSlide],
                        'page' => sprintf('page-%s', preg_replace('#\D#', '', $filename)),
                        'build_type' => 'flat'
                    )
                ),
                $buildDirectory,
                $localizedFilename
            );

            $progress->advance();
            $currentPageIndex = $currentPageIndex + 1;
        }
        $progress->finish();
        $output->writeln($this->messagesHelper->success_message('Build Completed'));
    }

    public function screenshot(InputInterface $input, OutputInterface $output) {}

    public function pdf(InputInterface $input, OutputInterface $output) {}

    public function package(InputInterface $input, OutputInterface $output) {}

} 
