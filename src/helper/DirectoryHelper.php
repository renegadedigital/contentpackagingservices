<?php

namespace Renegade\ContentPackagingServices\Helper;

use Symfony\Component\Filesystem\Filesystem;

class DirectoryHelper {

    /**
     * @var $filesystem \Symfony\Component\Filesystem\Filesystem
     */
    protected $filesystem;
    protected $config;
    protected $directory;

    /**
     * @param Filesystem $filesystem
     * @param $config
     * @param $directory
     */
    function __construct(Filesystem $filesystem, $config, $directory) {
        $this->filesystem = $filesystem;
        $this->config = $config;
        $this->directory = $directory;
    }

    /**
     * Initialize directories of a given type (localized)
     *
     * @param $language
     * @param $type
     * @return string Path to new directory
     */
    public function init($language, $type) {
        $localizedPath = $this->get_localized_dir($type, $language);
        $path = $this->get_dir($type);

        // Initialize the directory in $type if it doesn't exist
        if(!$this->filesystem->exists($path)) {
            $this->filesystem->mkdir($path);
        }

        // Remove the localized path if it exists
        if($this->filesystem->exists($localizedPath)) {
            $this->filesystem->remove($localizedPath);
        }

        $this->filesystem->mkdir($localizedPath);

        return realpath($localizedPath);
    }

    /**
     * Based on a given type return the localized directory
     *
     * @param $type
     * @param $locale
     * @return string
     */
    public function get_localized_dir($type, $locale) {
        return sprintf('%s/%s', $this->get_dir($type), $locale);
    }

    /**
     * Get a specified directory from the config
     *
     * @param $type
     * @return string
     */
    function get_dir($type) {
        switch($type) {
            case 'build':
                    return sprintf('%s/%s', $this->directory, $this->config['application']['build_dir']);
                break;

            case 'dist':
                    return sprintf('%s/%s', $this->directory, $this->config['application']['dist_dir']);
                break;

            case 'screenshots':
                    return sprintf('%s/%s', $this->directory, $this->config['application']['screenshots_dir']);
                break;

            case 'pdf':
                    return sprintf('%s/%s', $this->directory, $this->config['application']['pdf_dir']);
                break;
        }
    }

    /**
     * @param $contents
     * @param $path
     * @param $filename
     * @return bool|int
     */
    function save_page($contents, $path, $filename) {
        if(is_readable($path)) {
            return file_put_contents(sprintf('%s/%s', $path, $filename), $contents, LOCK_EX);
        } else {
            return false;
        }
    }

    

} 
